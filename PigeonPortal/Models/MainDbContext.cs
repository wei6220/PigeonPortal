﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PigeonPortal.Models
{
    public class MainDbContext : DbContext
    {
        public MainDbContext() : base("name=MainDbContext")
        {
        }
        public System.Data.Entity.DbSet<PigeonPortal.Models.Member> Members { get; set; }
    }
}