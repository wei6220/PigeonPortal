﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PigeonPortal.Models
{
    public class Member
    {
        [Column("Member_ID")]
        [Key, Required]
        public Guid ID { get; set; }

        [Column("Member_FirstName")]
        [Required, DisplayName("會員名字")]
        public string FirstName { get; set; }

        [Column("Member_LastName")]
        [Required, DisplayName("會員姓氏")]
        public string LastName { get; set; }

        [Column("Member_CountryName")]
        [DisplayName("國家")]
        public string CountryName { get; set; }

        [Column("Member_CityName")]
        [DisplayName("城市")]
        public string CityName { get; set; }

        [Column("Member_PostCode")]
        [DisplayName("郵遞區號")]
        public string PostCode { get; set; }

        [Column("Member_Address")]
        [DisplayName("地址")]
        public string Address { get; set; }

        [Column("Member_Tel")]
        [DisplayName("電話")]
        public string Tel { get; set; }

        [Column("Member_Mobile")]
        [Required, DisplayName("手機號碼")]
        public string Mobile { get; set; }

        [Column("Member_Email")]
        [DisplayName("電子郵件")]
        public string Email { get; set; }

        [Column("Account_PWD")]
        [DisplayName("會員密碼")]
        public string PWD { get; set; }

        //public virtual ICollection<Pigeonry> Pigeonries { get; set; }
        //public virtual ICollection<SystemRole> SystemRoles { get; set; }

        //組全名用的
        [NotMapped]
        public string MemberFullName
        {
            get
            {
                return this.FirstName + this.LastName;
            }
        }
    }
}